<?php

declare(strict_types=1);

namespace Drupal\inertia;

/**
 * Methods to render Inertia content.
 */
class Inertia {

  /**
   * Render an Inertia component.
   *
   * Should this accept context / &$variables as well?
   */
  public static function render(string $component, array $props = [], string $url = '', string $version = '', array $slots = []) {
    $data_page = [
      'component' => $component,
      'props' => $props,
      'url' => $url,
      'version' => $version,
    ];

    // @todo handle ID overrides
    // @todo handle library overrides.
    $build['content'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'app', 'data-page' => json_encode($data_page)],
      // '#attached' => array(
      // 'library' => array(
      // // @todo allow this method to accept a library override
      // 'inertia/app',
      // ),
      // ),
    ];

    foreach ($slots as $slot_name => $slot_content) {
      $build['content'][$slot_name] = [
        '#type' => 'html_tag',
        '#tag' => 'template',
        // @todo ID needs to be more specific.
        '#attributes' => ['name' => $slot_name],
        '#value' => $slot_content,
      ];
    }

    return $build;
  }

  // Should this accept overrides for component, props, etc.?
  // Also accept options array? Only option restricts fields displayed for
  // example.
  // $example_options = [
  // 'library' => [],.

  /**
   * Render an inertia component using the current template as context.
   */
  public static function renderByContext(&$variables, $options = []) {
    $props = [];
    $slots = [];
    $prop_keys = array_keys($variables['content']);
    foreach ($prop_keys as $key) {
      // @todo cases for SDCs, entities, etc.
      if ($variables['node']->hasField($key)) {
        $field = $variables['node']->get($key);
        $render_array = $field->view($variables['view_mode']);
        $rendered = \Drupal::service('renderer')->render($render_array);
        $props['props'][$key] = 'slot:' . $key;
        $slots[$key] = $rendered;
      }
    }

    // @todo think more about the best way to handle template suggestions.
    $variables['content'] = self::render(
      $variables['theme_hook_original'] . '--' . $variables['node']->getType() . '--' . $variables['view_mode'],
      $props,
      $variables['url'],
      '',
      $slots
    )['content'];
  }

}
