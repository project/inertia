## INTRODUCTION

Delegate rendering anywhere in Drupal's front end to your JavaScript framework
of choice. A Drupal-flavored implementation of https://inertiajs.com/

Still in early development - best bet to get hands on is likely this sandbox:
https://github.com/backlineint/drupal-inertia-sandbox

The inertia module primarily adds an implementation of Inertia's ::render()
method. This render method also introduces the concept of slots, which will map
nicely to Single Directory Components, but is not currently part of the main
Inertia implementation.

Additionally, the module provides a ::renderByContext() method that given the
$variables array for a particular template, will render all fields as slots.
This honors things like display mode controls in Drupal - re-order fields and
see the result in Inertia/React.

Equivalents will also be offered as Twig extensions, but are not yet implemented.

The current implementation only handles the initial page load and does not yet
support Inertia's client-side navigation for subsequent page loads.

## REQUIREMENTS

https://www.drupal.org/project/vite - for HMR in dev mode

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

TODO

## Development

### Scripts

- Composer scripts - can be run with `ddev composer <script>`
- ddev phpcs
- ddev phpcbf
- ddev eslint / ddev eslint --fix

#### phpstan

To run the phpstan composer script, you must currently first run

`ddev expand-composer-json`

This may eventually be resolved by:

https://github.com/ddev/ddev-drupal-contrib/pull/14

## MAINTAINERS

Current maintainers for Drupal 10:

- Brian Perry (brianperry) - https://www.drupal.org/u/brianperry
